#!/bin/sh

COMMAND=${1}
SUBCOMMAND=${2}
OPTIONS=${3}

docker-compose exec -T api sh -c "node ./node_modules/db-migrate/bin/db-migrate $COMMAND $SUBCOMMAND $OPTIONS"

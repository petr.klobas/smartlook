## Smartlook JS assignment

[Assignment](https://docs.google.com/document/d/1CL0pUA6QUQ7jV6EDXW-dIDUDfScKweeczVTiOMgGxlk/edit)

## Run the project

### Requirements
- Local installation of [Docker](https://www.docker.com/products/docker-desktop)

### Running
1. Copy `docker-compose.override.template` as `docker-compose.override.yml`, `.env.template` as `.env` and then run `docker-compose up --build`.
2. To initialize database structure run `bin/db-migrate.sh up` (database itself is created automatically in previous step)
3. First you must acquire JWT token, to be able to access API (`/token`), see doc/ for more info.
4. REST API is then available on your local machine on url: `http://localhost:8080` (if you used prepared template) you should see simple welcome message.
5. You can use prepared Postman collection to access endpoints, see `HackerNewsCollections.postman_collection.json`, or use OpenApi spec for you favourite client.
6. Happy coding / reviewing / using :)

### Development
- Project utilizes [nodemon](https://www.npmjs.com/package/nodemon) for better developer experience, so no restart / rebuild is needed for code changes in `src/` folder.

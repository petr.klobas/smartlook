#!/usr/bin/env sh

docker run --rm -v ${PWD}:/home/node/app node:slim /bin/sh -c "cd /home/node/app; npm install --no-audit; npm run generateOpenApi;"

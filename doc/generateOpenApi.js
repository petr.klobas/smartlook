const postmanToOpenApi = require('postman-to-openapi')

const projectRoot = process.cwd()
const targetFile = 'HackerNewsCollections.open-api.yml'
const postmanCollection = projectRoot  + '/HackerNewsCollections.postman_collection.json'
const outputFile = projectRoot + '/doc/' + targetFile

module.exports.generate = function () {

    postmanToOpenApi(postmanCollection, outputFile, {defaultTag: 'General'})
        .then(result => {
            console.log(`OpenAPI spec generated, check /doc/` + targetFile)
        })
        .catch(err => {
            console.log(err)
        })

}

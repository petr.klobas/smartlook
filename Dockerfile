FROM node:14-slim

WORKDIR /usr/src

COPY package*.json ./

RUN npm install --no-audit

COPY . .

EXPOSE 3000
ENV PORT 3000

ARG NODEBIN="node"
ENV NODEBIN ${NODEBIN}

RUN if [ "$NODEBIN" = "nodemon" ]; then \
    npm install -g nodemon --no-audit; \
  fi

CMD $NODEBIN server.ts

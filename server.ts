require('dotenv').config()

const app = require('./src/app')
const port = process.env.PORT || 3000

app.listen(port, () => {
    console.log(`HackerNews API listening at http://localhost:${port} (for docker env check your port mapping in docker-composer.yml)`)
})

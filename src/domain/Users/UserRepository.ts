import Postgres from "../../services/postgres";

export default class UserRepository {

    private readonly postgres

    constructor() {
        this.postgres = new Postgres()
    }

    async findById(id: number): Promise<any> {
        const res = await this.postgres.query('SELECT * FROM users WHERE id = $1', [id])

        if (res.rows.length !== 1) {
            throw new Error('User for id ' + id + ' not found')
        }

        return res.rows[0]
    }

}

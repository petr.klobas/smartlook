import Postgres from "../../services/postgres";
import jwt from "jsonwebtoken";

export default class UserFacade {

    private readonly postgres

    constructor() {
        this.postgres = new Postgres()
    }

    async authorize(email: string): Promise<string> {
        let res = await this.postgres.query('SELECT id FROM users WHERE email = $1', [email])

        if (res.rows.length === 0) {
            res = await this.postgres.query('INSERT INTO users (email) VALUES ($1) RETURNING id', [email])
        }

        return jwt.sign({'userId': res.rows[0].id}, process.env.JWT_SECRET, { expiresIn: '1800s' })
    }

}

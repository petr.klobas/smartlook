import Postgres from "../../services/postgres";
import StoryCollection from "./StroryCollection";
import StoryCollectionFactory from "./StoryCollectionFactory";

export default class StoryCollectionRepository {

    private readonly postgres: Postgres

    private readonly factory: StoryCollectionFactory

    constructor() {
        this.postgres = new Postgres()
        this.factory = new StoryCollectionFactory()
    }

    async findById(id: number): Promise<StoryCollection|null> {

        const raw = await this.postgres.query('SELECT * FROM collections WHERE id = $1', [id])

        if (raw.rows.length !== 1) {
            return null
        }

        const {rows} = await this.postgres.query(
            'SELECT s.*, cs.collection_id FROM stories s JOIN collections_x_stories cs ON s.hacker_news_id = cs.story_id ' +
            'WHERE cs.collection_id = $1',
            [id]
        )

        console.log(rows)

        const {name, owner_id } = raw.rows[0]

        return this.factory.create(id, name, owner_id, rows)
    }

}

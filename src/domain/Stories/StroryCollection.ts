import JsonSerialize from "../../app/JsonSerialize";

export default class StoryCollection implements JsonSerialize {
    id: number
    name: string
    ownerId: number
    stories: Array<object>

    constructor(id, name, ownerId, stories) {
        this.id = id
        this.name = name
        this.ownerId = ownerId
        this.stories = stories
    }

    toJson(): object {
        return {
            'id': this.id,
            'name': this.name,
            'owner': this.ownerId,
            'stories': this.stories
        };
    }

}

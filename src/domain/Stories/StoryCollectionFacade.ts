import Postgres from "../../services/postgres";
import got from "got";
import Elastic from "../../services/elastic";

export default class StoryCollectionFacade {

    private readonly postgres: Postgres
    private readonly elastic: Elastic
    private readonly storySourceTemplate: string

    constructor() {
        this.postgres = new Postgres()
        this.elastic = new Elastic()
        this.storySourceTemplate = 'https://hacker-news.firebaseio.com/v0/item/{storyId}.json'
    }

    async add(name: string, owner: number): Promise<any> {
        const res = await this.postgres.query(
            'INSERT INTO collections (name, owner_id) VALUES ($1, $2) RETURNING id',
            [name, owner]
        )

        return res.rows[0].id
    }

    async edit(id: number, name: string): Promise<any> {
        const res = await this.postgres.query(
            'UPDATE collections SET name = $1 WHERE id = $2 RETURNING id',
            [name, id]
        )

        return res.rows[0].id
    }

    async delete(id: number): Promise<any> {
        const res = await this.postgres.query(
            'DELETE FROM collections WHERE id = $1 RETURNING id',
            [id]
        )

        return res.rows[0].id
    }

    async push(collectionId: number, storyIds: Array<number>): Promise<any[]> {

        if (await this.elastic.indexExists() === false) {
            await this.elastic.createIndex()
        }

        return this.processHackerNewsObjects(collectionId, storyIds)
    }

    async processHackerNewsObjects(collectionId: number, objectIds: Array<number>): Promise<any[]> {

        const objects = await this.prepareFetches(objectIds)

        const persists = []
        objects.forEach((object) => {
            const {id, type, by, time, text = null, parent = null, kids = [], url = null, title = null} = object

            persists.push(this.postgres.query(
                'INSERT INTO stories (hacker_news_id, type, author, posted_at, title, content, url, parent) ' +
                'VALUES ($1, $2, $3, $4, $5, $6, $7, $8)' +
                'ON CONFLICT (hacker_news_id) DO UPDATE SET posted_at = $9, title = $10, content = $11, url = $12'
                ,
                [id, type, by, time, title, text, url, parent, time, title, text, url]
            ))

            persists.push(this.elastic.client.update({
                index: 'hacker_news_stories',
                id: id.toString(),
                body: {
                    doc: {
                        author: by,
                        posted_at: time,
                        title: title,
                        content: text,
                        url: url,
                        type: type
                    },
                    doc_as_upsert: true
                }
            }))

            if (type === 'story') {
                persists.push(this.postgres.query(
                    'INSERT INTO collections_x_stories (collection_id, story_id) VALUES ($1, $2) ON CONFLICT DO NOTHING',
                    [collectionId, id]
                ))
            }

            if (kids.length > 0) {
                persists.push(this.processHackerNewsObjects(collectionId, kids))
            }
        })

        return Promise.all(persists)
    }

    prepareFetches(objectIds: Array<number>): Promise<HackerNewsObject[]> {
        const fetches = []
        objectIds.forEach((storyId: number) => {
            const storyUrl = this.storySourceTemplate.replace('{storyId}', storyId.toString())
            fetches.push(this.fetchHackerNewsObject(storyUrl))
        })

        return Promise.all(fetches)
    }

    fetchHackerNewsObject(url: string): Promise<HackerNewsObject> {
        return new Promise<HackerNewsObject>((resolve, reject) => {
            got(url).then((res) => resolve(JSON.parse(res.body)))
        });
    }

    async search(phrase: string): Promise<SearchResult[]> {
        const {body} = await this.elastic.search(phrase)

        if (body.hits.hits.length === 0) {
            return []
        }

        const searchResults = []

        for (let i = 0; i < body.hits.hits.length; i++) {
            const {_id, _source} = body.hits.hits[i]
            let {author, title, content, url, type} = _source
            let story = _id

            if (type === 'comment') {
                const {rows} = await this.findStoryForComment(_id)
                url = rows[0].url
                story = rows[0].hacker_news_id
            }

            searchResults.push({
                type: type,
                author: author,
                title: title,
                content: content,
                story: story,
                url: url
            })
        }

        return searchResults
    }

    async findStoryForComment(commentId: number): Promise<any> {
        return this.postgres.query('WITH RECURSIVE commentForStory AS (' +
            'SELECT * FROM stories WHERE hacker_news_id = $1 ' +
            'UNION ALL ' +
            'SELECT stories.* FROM stories JOIN commentForStory ON commentForStory.parent = stories.hacker_news_id) ' +
            'SELECT * FROM commentForStory WHERE parent IS NULL', [commentId]
        )
    }

}

type HackerNewsObject = {
    id: number,
    type: string,
    by: string,
    time: number,
    text: string,
    parent: number,
    kids: Array<number>,
    url: string,
    title: string
}

type SearchResult = {
    type: string,
    author: string,
    title?: string,
    content?: string,
    story: number,
    url: string
}

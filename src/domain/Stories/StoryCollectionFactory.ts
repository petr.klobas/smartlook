import StoryCollection from "./StroryCollection";

export default class StoryCollectionFactory {

    create(id: number, name: string, owner: number, stories): StoryCollection {
        stories.map((story) => {
            delete story.parent
            delete story.collection_id
            delete story.content

            return story
        })

        return new StoryCollection(id, name, owner, stories)
    }

}

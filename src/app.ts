import express from 'express'
import createError from 'http-errors'
import checkJwt from 'express-jwt'
import {router as indexRouter} from './routes/index'
import {router as collectionRouter} from './routes/storyCollections'

const app = express()

app.use(express.json())

// authorization
app.use(checkJwt({ secret: process.env.JWT_SECRET, algorithms: ['HS256'] }).unless({path: ['/token']}));

// routing
app.use('/', indexRouter)
app.use('/collections', collectionRouter)

// catch 404 and forward to error handler
app.use((req, res, next) => {
    next(createError(404));
});

// error handler
app.use((err, req, res, next) => {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.send('error')
})

module.exports = app

import express from 'express'
import {body, param, validationResult, CustomValidator} from 'express-validator'

import SuccessResponse from "../app/SuccessResponse"
import StoryCollectionFacade from "../domain/Stories/StoryCollectionFacade";
import ErrorResponse from "../app/ErrorResponse";
import StoryCollectionRepository from "../domain/Stories/StoryCollectionRepository";

export const router = express.Router()

const collectionFacade = new StoryCollectionFacade()
const collectionRepo = new StoryCollectionRepository()
const collectionAccessValidator: CustomValidator = async (id, meta) => {

    const collection = await collectionRepo.findById(id)

    const {userId} = meta.req.user

    if (collection === null) {
        return Promise.reject('Collection does not exist')
    }

    if (collection.ownerId !== userId) {
        return Promise.reject('Collection does not belong to you')
    }

}

router.get('/:id',
    param('id').custom(collectionAccessValidator),
    async (req, res) => {

    validatePayload(res, req)

    const { id } = req.params
    const collection = await collectionRepo.findById(id)

    res.json(collection.toJson())
})

router.post(
    '/',
    body('name').isString(),
    async (req, res) => {

        validatePayload(res, req)

        const { userId } = req.user

        try {
            const colId = await collectionFacade.add(req.body.name, userId)
            const responseBody = {... new SuccessResponse('Story collection created').toJson(), id: colId}
            res.status(201).json(responseBody)
        } catch (err) {
            const message = err.code === '23505' ? 'Collection already exists' : err.toString()
            const code = err.code === '23505' ? 400 : 500

            return res.status(code).json(new ErrorResponse(code, message).toJson())
        }
})

router.put(
    '/:id',
    param('id').custom(collectionAccessValidator),
    body('name').isString(),
    async (req, res) => {
        validatePayload(res, req)
        const {id} = req.params
        const {name} = req.body

        try {
            await collectionFacade.edit(id, name)
        } catch (err) {
            return res.status(500).json(new ErrorResponse(500, err.toString()).toJson())
        }

        res.status(200).json(new SuccessResponse('Story collection updated').toJson())
    }
)

router.delete(
    '/:id',
    param('id').custom(collectionAccessValidator),
    async (req, res) => {
        validatePayload(res, req)

        const {id} = req.params

        try {
            await collectionFacade.delete(id)
        } catch (err) {
            return res.status(500).json(new ErrorResponse(500, err.toString()).toJson())
        }

        res.status(200).json(new SuccessResponse('Story collection deleted').toJson())
    }
)


router.post(
    '/:id/add',
    param('id').custom(collectionAccessValidator),
    body('stories').isArray(),
    async (req, res) => {
        validatePayload(res, req)

        const {stories} = req.body
        const {id} = req.params

        try {
            await collectionFacade.push(id, stories)
        } catch (err) {
            return res.status(500).json(new ErrorResponse(500, err.toString()).toJson())
        }

        res.status(200).json(new SuccessResponse('Stories added to collection').toJson())
    }
)

router.get(
    '/search/:phrase',
    param('phrase').isString().isLength({min: 3}),
    async (req, res) => {
        validatePayload(res, req)

        const {phrase} = req.params

        try {
            let searchResults = await collectionFacade.search(phrase)
            res.status(200).json(searchResults)
        } catch (err) {
            return res.status(500).json(new ErrorResponse(500, err.toString()).toJson())
        }
    }
)


function validatePayload(res, req): void {
    const errors = validationResult(req);
    if ( ! errors.isEmpty()) {
        return res.status(400).json({errors: errors.array()});
    }
}


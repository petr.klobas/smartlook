import Router from 'express-promise-router'

import UserFacade from "../domain/Users/UserFacade"
import UserRepository from "../domain/Users/UserRepository";

export const router = Router()

router.get('/', async (req, res, next) => {

    const userRepo = new UserRepository()
    const {userId} = req.user

    const user = await userRepo.findById(userId)

    res.send('HackerNews collection API, welcome ' + user.email + '!')
})

router.post('/token', async (req, res, next) => {

    const userFacade = new UserFacade()

    try {
        const token = await userFacade.authorize(req.body.user)
        res.json(token)
    } catch (e) {
        res.status(500).json({'state': 'error', 'message': e.message})
    }

})

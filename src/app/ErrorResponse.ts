import {ResponseBody} from "./Response"
import JsonSerialize from "./JsonSerialize";

export default class ErrorResponse implements JsonSerialize {

    public message: string

    public code: number

    constructor(code: number, message: string) {
        this.code = code
        this.message = message

    }

    toJson(): ResponseBody {
        return {
            status: "error",
            message: this.message
        }
    }

}

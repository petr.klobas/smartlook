export type ResponseBody = {
    status: string
    message: string
}

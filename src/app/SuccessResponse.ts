import {ResponseBody} from "./Response"
import JsonSerialize from "./JsonSerialize";

export default class SuccessResponse implements JsonSerialize {

    private readonly message: string

    constructor(message?: string) {
        this.message = message ?? 'succeeded';
    }

    toJson(): ResponseBody {
        return {
            status: "ok",
            message: this.message
        }
    }

}

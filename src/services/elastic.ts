import {Client} from "@elastic/elasticsearch";

export default class Elastic {

    client: Client

    private readonly storiesIndex: string

    constructor() {
        this.client = new Client({node: 'http://elastic:9200'})
        this.storiesIndex = 'hacker_news_stories'
    }

    async indexExists(): Promise<boolean> {
        const {body} = await this.client.indices.exists({index: this.storiesIndex})

        return body
    }

    async search(phrase): Promise<any> {
        return this.client.search({
            index: this.storiesIndex,
            body: {
                query: {
                    query_string: {
                        query: phrase,
                        fields: ['title^2', 'content^1.5', 'author', 'url']
                    }
                }
            }
        })
    }

    async createIndex() {
         await this.client.indices.create({
            'index': this.storiesIndex,
            'body': {
                settings: {
                    number_of_shards: 3,
                    number_of_replicas: 2,
                },
                mappings: {
                    properties: {
                        title: {
                            type: 'text',
                            analyzer: 'english'
                        },
                        content: {
                            type: 'text',
                            analyzer: 'english'
                        },
                        author: {
                            type: 'keyword'
                        },
                        url: {
                            type: 'keyword',
                            ignore_above : 2000
                        }
                    }
                }
            }
        })
    }

}

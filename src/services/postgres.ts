import {Client} from 'pg';

require('dotenv').config()

const client = new Client({
    host: process.env.POSTGRES_HOST,
    user: process.env.POSTGRES_USER,
    password: process.env.POSTGRES_PASSWORD,
    database: process.env.POSTGRES_DATABASE
})

async function connect() {
    try {
        await client.connect()
    } catch (err) {
        console.error('connection error', err.stack)
    }
}

connect();

export default class Postgres {
    async query(sql: string, params?: Array<any>) {
        return client.query(sql, params)
    }
}

declare namespace Express {

     interface User {
        userId: number
        iat: number
        exp: number
    }

}

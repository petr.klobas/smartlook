CREATE TABLE "stories" (
   "hacker_news_id" integer NOT NULL,
   PRIMARY KEY ("hacker_news_id"),
   "type" character varying(255) NOT NULL,
   "author" character varying(255) NULL,
   "posted_at" integer NOT NULL,
   "title" character varying(1000) NULL,
   "content" text NULL,
   "url" character varying(2048) NULL,
   "parent" integer NULL
);

ALTER TABLE "stories"
    ADD FOREIGN KEY ("parent") REFERENCES "stories" ("hacker_news_id") ON DELETE CASCADE ON UPDATE CASCADE;

CREATE TABLE "collections_x_stories" (
    "collection_id" integer NOT NULL,
    "story_id" integer NOT NULL
);

ALTER TABLE "collections_x_stories"
    ADD FOREIGN KEY ("collection_id") REFERENCES "collections" ("id") ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE "collections_x_stories"
    ADD FOREIGN KEY ("story_id") REFERENCES "stories" ("hacker_news_id") ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE "collections_x_stories"
    ADD CONSTRAINT "collections_x_stories_collection_id_story_id" UNIQUE ("collection_id", "story_id");

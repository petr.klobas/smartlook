CREATE TABLE "users"
(
    "id" serial NOT NULL,
    PRIMARY KEY ("id"),
    "email" character varying(255) NOT NULL
);

ALTER TABLE "users"
    ADD CONSTRAINT "users_email" UNIQUE ("email");

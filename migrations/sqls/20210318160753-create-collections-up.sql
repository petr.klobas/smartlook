CREATE TABLE "collections"
(
    "id" serial NOT NULL,
    PRIMARY KEY ("id"),
    "name" character varying(255) NOT NULL,
    "owner_id" integer NOT NULL,
    UNIQUE ("name", "owner_id")
);

ALTER TABLE "collections"
    ADD FOREIGN KEY ("owner_id") REFERENCES "users" ("id") ON DELETE CASCADE ON UPDATE CASCADE;
